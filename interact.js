var boxStyle =  new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(255, 0, 0, 0.3)'
  }),
  stroke: new ol.style.Stroke({ 
    color: '#FF0000',
    width:2
  }) 
});

var dragBox = new ol.interaction.DragBox({
  style: boxStyle
});
    
var overlay = new ol.FeatureOverlay({
  map: map,
  style: function(feature, resolution) {
    return [boxStyle];
  }
});

// Listeners dragbox interaction event
dragBox.on('boxstart', function(evt) {
  overlay.getFeatures().clear();
});
                
dragBox.on('boxend', function(evt) {
  var bbox = dragBox.getGeometry().getExtent();
  var count = 0;
  $('#parcels-nav li').each(function() {
    parcel = $(this).data('parcel');
    if (typeof parcel === 'undefined') return;
    attrs = parcel['location']['data']['attrs'];
    /*console.log(attrs.x, bbox[1], bbox[3]);*/
    if (attrs.x > bbox[1] && attrs.x < bbox[3] &&
        attrs.y > bbox[0] && attrs.y < bbox[2]) {
      $(this).show();
      count++;
    } else {
      $(this).hide();
    }
  });
  overlay.addFeature(new ol.Feature(dragBox.getGeometry()));
  map.removeInteraction(dragBox);
  $("#map").removeClass("drawing");

  $("#status").html(count + ' notice(s) selected');
  if (count == 0) {
    $('.viewer-subscribe').addClass('hide');
  } else {
    $('.viewer-subscribe').removeClass('hide');
  }
});

// Subscribe button
$('.viewer-subscribe').click(function() {
  window.prompt('Enter your email address to subscribe for updates to this area');
});

// Add new rectangle link click event
$(".viewer-new-rectangle").click(function(e){
  $("#map").addClass("drawing");
  e.preventDefault();
  if (dragBox) {
    map.removeInteraction(dragBox);
  }
  map.addInteraction(dragBox);
});

////////////////

if (false) // UNDER CONSTRUCTION
{
// select interaction working on "singleclick"
var selectSingleClick = new ol.interaction.Select();

// select interaction working on "click"
var selectClick = new ol.interaction.Select({
  condition: ol.events.condition.click
});

var select = selectSingleClick;  // ref to interaction

map.addInteraction(select);
select.on('select', function(e) {
  $('#status').html('&nbsp;' + e.target.getFeatures().getLength() +
      ' selected features (last operation selected ' + e.selected.length +
      ' and deselected ' + e.deselected.length + ' features)');
});

}