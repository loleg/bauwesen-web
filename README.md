# Building Permits application frontend #

This is a prototype application. The goal is to create the basis for an application for tracking building permits and other zoning related information from a commune, and creating the basis for maps, notifications and searchable resources for users at a later stage.

### Running ###

If you have Python 2.x installed, testing can be done by running this command at the root folder:
```
python -m SimpleHTTPServer
```
Otherwise, just deploy the project into a web server.

### Developer contact ###

http://datalets.ch