var attributions = [
  new ol.Attribution({
    html: '<a href="http://www.geo.admin.ch/internet/geoportal/en/home.html">' +
      '&copy swisstopo / Amtliche Vermessung Schweiz/FL</a>'
  })
];

var resolutions = [
  4000, 3750, 3500, 3250, 3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250,
  1000, 750, 650, 500, 250, 100, 50, 20, 10, 5, 2.5, 2, 1.5, 1, 0.5, 0.25, 0.1
];

var wmsCadastre = new ol.layer.Tile({
  extent: [420000, 30000, 900000, 350000],
  source: new ol.source.TileWMS({
    url: 'http://wms.geo.admin.ch/',
    crossOrigin: 'anonymous',
    attributions: attributions,
    params: {
      'LAYERS': 'ch.kantone.cadastralwebmap-farbe',
      'FORMAT': 'image/png',
      'TILED': true,
      'VERSION': '1.1.1'
    },
    serverType: 'mapserver'
  })
});

// Set up the base map
var map = new ga.Map({
  target: 'map',
  layers: [wmsCadastre],
  view: new ol.View({
    resolution: 1.0,
    center: [685087, 246755]
  })
});

// Get locations of parcels
var cacheLocationData = function(parcel, query, byParcel) {
  if (typeof byParcel === 'undefined') byParcel = false;
  $.get(
    'https://api3.geo.admin.ch/rest/services/api/SearchServer?' +
    'searchText=' + query +
    (byParcel ? '&origins=parcel' : '') +
    '&type=locations',
    function(locations) {
      console.log("Logging results for " + query);
      if (locations.results.length == 0) { console.log("No results!"); return; }
      if (locations.results.length > 1) window.alert("Multiple results!");
      console.log(JSON.stringify(locations.results[0]));
      parcel['location']['data'] = locations.results[0];
      displayLabel(parcel);
    }
  );
}

var parseExtent = function(stringBox2D) {
  var extent = stringBox2D.replace('BOX(', '').replace(')', '').replace(',', ' ').split(' ');
  return $.map(extent, parseFloat);
};

// When a result is selected.
var zoomToLocation = function(location) {
  var originZoom = {
    address: 10,
    parcel: 10,
    sn25: 8,
    feature: 7
  };
  var view = map.getView();
  var origin = location.attrs.origin;
  var extent = [0,0,0,0];
  if(location.attrs.geom_st_box2d) {
    extent = parseExtent(location.attrs.geom_st_box2d);
  } else if (location.attrs.x && location.attrs.y) {
    var x = location.attrs.y;
    var y = location.attrs.x
    extent = [x,y,x,y];
  }
  if(originZoom.hasOwnProperty(origin)) {
    var zoom = originZoom[origin];
    var center = [(extent[0] + extent[2]) / 2, (extent[1] + extent[3]) / 2];
    view.setZoom(zoom);
    view.setCenter(center);
  } else {
    view.fitExtent(extent, map.getSize());
  }
}

// Initialize the location marker
var parcels = [];
var displayLabel = function(parcel) {
  parcels.push(parcel);
  var location = parcel['location']['data'];
  // Create a new popup
  var element = $('<div class="marker">' +
      parcel['location']['address'] + ' ' +
      parcel['location']['parcel'] +
    '</div>');
  var popup = new ol.Overlay({
    positioning: 'centre-center',
    element: element
  });
  map.addOverlay(popup);
  // Create a navigation button
  $('#parcels-nav').append(
    '<li class="table-view-cell">' +
      '<a class="navigate-right">' +
        '<span class="badge">' + 
          parcel['date']['announced'] +
        '</span>' +
        parcel['title'] +
      '</a>' +
      '<p style="display:none">' +
        parcel['text'] +
      '</p>' +
    '</li>'
    ).find('li:last-child').click(function() {
      zoomToLocation(location);
      $(this).parent().find('p').hide();
      $(this).find('p').show();
    }).data('parcel', parcel);
  // Calculate the coordinates
  var origin = location.attrs.origin;
  var extent = [0,0,0,0];
  if(location.attrs.geom_st_box2d) {
    extent = parseExtent(location.attrs.geom_st_box2d);
  } else if (location.attrs.x && location.attrs.y) {
    var x = location.attrs.y;
    var y = location.attrs.x
    extent = [x,y,x,y];
  }
  var center = [(extent[0] + extent[2]) / 2, (extent[1] + extent[3]) / 2];
  popup.setPosition(center);
}

// Load and parse data file
$.getJSON('data.json', function(data) {
  $.each(data, function() {
    loc = this['location'];
    if (typeof loc === "undefined") return;
    // Add to datastore
    if (typeof loc['data'] !== "undefined") {
      displayLabel(this);
      return;
    }
    // No cached data, parse and log
    if (typeof loc['city'] === "undefined" ||
               loc['city'] == "") return;
    if (typeof loc['parcel'] === "undefined" ||
               loc['parcel'] == "") {
      if (typeof loc['address'] === "undefined" ||
                 loc['address'] == "") return;
      cacheLocationData(this,
        loc['address'] + ', ' + 
        loc['city']
      );
    } else {
      cacheLocationData(this,
        loc['city'] + ' ' + 
        loc['parcel'],
        true
      );
    }
  });
  // Zoom to first parcel
  zoomToLocation(parcels[0].location.data);
})
.fail(function(jqXHR, textStatus, errorThrown) { alert('getJSON error: ' + textStatus); });

// Clear the splash
setTimeout(function() { $('#splash').addClass('fadeout') }, 1000);
setTimeout(function() { $('#splash').remove() }, 3000);
